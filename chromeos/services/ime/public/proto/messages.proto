// Messages sent between the IME service and the shared library.
// Although Chrome communicates with the IME service using Mojo, the shared
// library is built in google3, which doesn't support Mojo yet. Hence, when the
// IME service sends or receives messages via Mojo, it needs to convert the Mojo
// calls into protos before forwarding to and from the shared library:
//
//      [Chrome] <---Mojo---> [IME Service] <---Proto---> [Shared Library]

syntax = "proto2";

package chromeos.ime;

// The base message type for all communication between the IME service and the
// shared library. The IME service and the shared library uses both a public and
// private protocol, so Wrapper has fields for both. For the private messages,
// Wrapper stores it as a serialized proto to keep the message private.
message Wrapper {
  oneof param {
    PublicMessage public_message = 1;
    bytes private_message = 2;
  }
}

// Public messages between IME service and the shared library.
// Each 'oneof' submessage represents a Mojo call / response.
message PublicMessage {
  optional int32 seq_id = 1;

  oneof param {
    OnFocus on_focus = 2;
    OnKeyEvent on_key_event = 3;
    OnKeyEventReply on_key_event_reply = 4;
    OnSurroundingTextChanged on_surrounding_text_changed = 5;
    OnBlur on_blur = 6;
    OnCompositionCanceled on_composition_canceled = 7;
    OnInputMethodChanged on_input_method_changed = 8;
    CommitText commit_text = 9;
    SetComposition set_composition = 10;
    SetCompositionRange set_composition_range = 11;
    FinishComposition finish_composition = 12;
    DeleteSurroundingText delete_surrounding_text = 13;
    HandleAutocorrect handle_autocorrect = 14;
    SuggestionsRequest suggestions_request = 15;
    SuggestionsResponse suggestions_response = 16;
  }
}

// Protobuf version of the reply from InputEngine::InputFieldInfo in
// chromeos/services/ime/public/mojom/input_engine.mojom
message InputFieldInfo {
  enum InputFieldType {
    INPUT_FIELD_TYPE_UNSPECIFIED = 0;  // Reserved
    INPUT_FIELD_TYPE_NO_IME = 1;
    INPUT_FIELD_TYPE_TEXT = 2;
    INPUT_FIELD_TYPE_SEARCH = 3;
    INPUT_FIELD_TYPE_TELEPHONE = 4;
    INPUT_FIELD_TYPE_URL = 5;
    INPUT_FIELD_TYPE_EMAIL = 6;
    INPUT_FIELD_TYPE_NUMBER = 7;
    INPUT_FIELD_TYPE_PASSWORD = 8;
  }

  enum AutocorrectMode {
    AUTOCORRECT_MODE_UNSPECIFIED = 0;  // Reserved
    AUTOCORRECT_MODE_DISABLED = 1;
    AUTOCORRECT_MODE_ENABLED = 2;
  }

  enum PersonalizationMode {
    PERSONALIZATION_MODE_UNSPECIFIED = 0;  // Reserved
    PERSONALIZATION_MODE_DISABLED = 1;
    PERSONALIZATION_MODE_ENABLED = 2;
  }

  optional InputFieldType type = 1;
  optional AutocorrectMode autocorrect = 2;
  optional PersonalizationMode personalization = 3;
}

// Protobuf version of InputEngine::OnInputMethodChanged in
// chromeos/services/ime/public/mojom/input_engine.mojom
message OnInputMethodChanged {
  optional string engine_id = 1;
}

// Protobuf version of InputEngine::OnFocus in
// chromeos/services/ime/public/mojom/input_engine.mojom
message OnFocus {
  optional InputFieldInfo info = 1;
}

// Protobuf version of InputEngine::OnBlur in
// chromeos/services/ime/public/mojom/input_engine.mojom
message OnBlur {}

// Protobuf version of InputEngine::OnKeyEvent in
// chromeos/services/ime/public/mojom/input_engine.mojom
message OnKeyEvent {
  optional PhysicalKeyEvent key_event = 1;
}

// Protobuf version of the reply from InputEngine::OnKeyEvent in
// chromeos/services/ime/public/mojom/input_engine.mojom
message OnKeyEventReply {
  optional bool consumed = 1;
}

// Protobuf version of InputEngine::PhysicalKeyEvent in
// chromeos/services/ime/public/mojom/input_engine.mojom
message PhysicalKeyEvent {
  enum EventType {
    EVENT_TYPE_UNSPECIFIED = 0;
    EVENT_TYPE_KEY_DOWN = 1;
    EVENT_TYPE_KEY_UP = 2;
  }

  optional EventType type = 1;
  optional string code = 2;
  optional string key = 3;
  optional ModifierState modifier_state = 4;
}

// Protobuf version of InputEngine::PhysicalKeyEvent in
// chromeos/services/ime/public/mojom/input_engine.mojom
message ModifierState {
  optional bool alt = 1;
  optional bool alt_graph = 2;
  optional bool caps_lock = 3;
  optional bool control = 4;
  optional bool meta = 5;
  optional bool shift = 6;
}

// Protobuf version of InputEngine::SelectionRange in
// chromeos/services/ime/public/mojom/input_engine.mojom
message SelectionRange {
  optional uint32 anchor = 1;
  optional uint32 focus = 2;
}

// Protobuf version of gfx.mojom.Range in
// ui/gfx/range/mojom/range.mojom
message Range {
  optional uint32 start = 1;
  optional uint32 end = 2;
}

// Protobuf version of InputEngine::AutocorrectSpan in
// chromeos/services/ime/public/mojom/input_engine.mojom
message AutocorrectSpan {
  optional Range autocorrect_range = 1;
  optional string original_text = 2;
};

// Protobuf version of InputEngine::OnSurroundingTextChanged in
// chromeos/services/ime/public/mojom/input_engine.mojom
message OnSurroundingTextChanged {
  optional string text = 1;
  optional uint32 offset = 2;
  optional SelectionRange selection_range = 3;
}

// Protobuf version of InputEngine::OnCompositionCanceled in
// chromeos/services/ime/public/mojom/input_engine.mojom
message OnCompositionCanceled {}

// Protobuf version of InputEngine::CommitText in
// chromeos/services/ime/public/mojom/input_engine.mojom
message CommitText {
  optional string text = 1;
}

// Protobuf version of InputEngine::SetComposition in
// chromeos/services/ime/public/mojom/input_engine.mojom
message SetComposition {
  optional string text = 1;
}

// Protobuf version of InputEngine::SetCompositionRange in
// chromeos/services/ime/public/mojom/input_engine.mojom
message SetCompositionRange {
  optional uint32 start_byte_index = 1;
  optional uint32 end_byte_index = 2;
}

// Protobuf version of InputEngine::FinishComposition in
// chromeos/services/ime/public/mojom/input_engine.mojom
message FinishComposition {}

// Protobuf version of InputEngine::DeleteSurroundingText in
// chromeos/services/ime/public/mojom/input_engine.mojom
message DeleteSurroundingText {
  optional uint32 num_bytes_before_cursor = 1;
  optional uint32 num_bytes_after_cursor = 2;
}

// Protobuf version of InputEngine::HandleAutocorrect in
// chromeos/services/ime/public/mojom/input_engine.mojom
message HandleAutocorrect {
  optional AutocorrectSpan autocorrect_span = 1;
}

// Protobuf version of CompletionCandidate in
// chromeos/services/ime/public/mojom/input_engine.mojom
message CompletionCandidate {
  optional string text = 1;
  optional float normalized_score = 2;
}

// Protobuf version of SuggestionsRequest in
// chromeos/services/ime/public/mojom/input_engine.mojom
message SuggestionsRequest {
  optional string text = 1;
  repeated CompletionCandidate completion_candidates = 2;
}

// Protobuf version of MultiWordSuggestionCandidate in
// chromeos/services/ime/public/mojom/input_engine.mojom
message MultiWordSuggestionCandidate {
  optional string text = 1;
  optional float normalized_score = 2;
}

// Protobuf version of SuggestionCandidate in
// chromeos/services/ime/public/mojom/input_engine.mojom
message SuggestionCandidate {
  oneof candidate { MultiWordSuggestionCandidate multi_word = 1; }
}

// Protobuf version of SuggestionsResponse in
// chromeos/services/ime/public/mojom/input_engine.mojom
message SuggestionsResponse {
  repeated SuggestionCandidate candidates = 1;
}
