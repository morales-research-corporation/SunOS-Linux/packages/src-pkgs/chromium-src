// Copyright 2020 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "chrome/browser/continuous_search/features.h"

namespace continuous_search {
namespace features {

const base::Feature kContinuousSearch{"ContinuousSearch",
                                      base::FEATURE_DISABLED_BY_DEFAULT};

}  // namespace features
}  // namespace continuous_search
