This directory contains code used by Chrome Enterprise Connectors. This
includes:
  * Code to handle Connector policies.
  * Code specific to uploaded content analysis.
  * Common utilities used by Connector code.

Code shared with user downloads cloud scanning should be added to
`//chrome/browser/safe_browsing/cloud_content_scanning/` instead.

Code specific to user downloads cloud scanning should be added to
`//chrome/browser/safe_browsing/download_protection/` instead.
