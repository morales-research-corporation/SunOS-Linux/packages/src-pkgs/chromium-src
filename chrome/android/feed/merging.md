# Manual Merges

[Piet code](https://cs.chromium.org/chromium/src/chrome/android/feed/core/java/src/org/chromium/chrome/browser/feed/library/piet/)
and [feed wire protos](https://cs.chromium.org/chromium/src/components/feed/core/proto/wire/)
are hand-merged from https://chromium.googlesource.com/feed git repo regularly.
Commits that don't touch these files do not cause changes here.

The hash below represents the last commit from that repo that was reviewed for
the potential need to merge here.

Last checked commit ID: c6eb65cc4e080d96c453c677df9bf58b3abcb9b8
