// Copyright 2016 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "storage/browser/quota/quota_features.h"

namespace storage {

namespace features {

// Enables Storage Pressure Event.
const base::Feature kStoragePressureEvent{"StoragePressureEvent",
                                          base::FEATURE_DISABLED_BY_DEFAULT};

}  // namespace features
}  // namespace storage
